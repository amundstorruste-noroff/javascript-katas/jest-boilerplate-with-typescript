import { clockwiseCipher } from "./index";
import theoretically from "jest-theories";

describe("clockwiseCipher", () => {
    const theories = [
        {
            input: "Mubashir Hassan",
            expected: "Ms ussahr nHaaib"
        },
        {
            input: "Mubashir Hassan",
            expected: "Ms ussahr nHaaib"
        },
        {
            input: "Matt MacPherson",
            expected: "M ParsoMc nhteat"
        },
        {
            input: "Edabit is amazing",
            expected: "Eisadng  tm    i   zbia a"
        },
        {
            input: "abcdefghijklmnopqrstuvwxyz",
            expected: "aeimqbtuy vfp   zjl    nhx  wrdsokgc"
        },
        {
            input: "It's time to go on an adventure",
            expected: "I e otanvu t    e ot  rgmdenans oti'"
        }
    ];
    theoretically(
        "should convert the input: '{input}' into the the following: '{expected}'",
        theories,
        (theory) => {
            // Act
            const output = clockwiseCipher(theory.input);
            // Assert
            expect(output).toEqual(theory.expected);
        }
    );
});
