export const clockwiseCipher = (input: string) => {
    let message = [...input]; // Copy input

    // Identify size (1, 4, 16, 26, ...)
    const stringLength = message.length;

    // Calculate necessary matrix size
    let width = 1;
    while (width ** 2 < stringLength) {
        width += 1;
    }

    // Calculate number of "rings"
    const numberOfRings = Math.ceil(width / 2);

    // Generate Cipher matrix
    let matrix = [];
    for (let i = 0; i < width; i++) {
        matrix.push(new Array(width).fill(-1));
    }

    // Pad message to correct length
    message = padMessage(message, width ** 2);

    // For each ring. Insert the message according to the algorithm
    for (let ring = 0; ring < numberOfRings; ring++) {
        const ringWidth = width - ring * 2;
        const startIdx = ring;
        const endIdx = startIdx + ringWidth - 1;

        message = reorderSlice(message, ringWidth);
        matrix = fillRing(message, matrix, ring, endIdx);
    }

    // Get the encoded string from the Clockwise Ciphered matrix
    const encodedString = getEncodedStringFromMatrix(matrix);

    return encodedString;
};

// Fill a specific ring in a matrix with a message
const fillRing = (
    message: string[],
    matrix: any,
    start: number,
    end: number
) => {
    // Stop early if ring is 1x1
    if (start === end) {
        matrix[start][end] = message.shift();
        return matrix;
    }
    // for (let col = ringIdx; ringIdx < )
    // [r, 1 --> end-1]
    for (let col = start; col < end; col++) {
        matrix[start][col] = message.shift();
    }

    // Fill right
    // [r --> end-1, end]
    for (let row = start; row < end; row++) {
        matrix[row][end] = message.shift();
    }

    // Fill bottom
    // [end, end --> 2 (reverserd)]
    for (let col = end; col > start; col--) {
        matrix[end][col] = message.shift();
    }

    // Fill left
    // [end --> 2 (reversed), 1]
    for (let row = end; row > start; row--) {
        matrix[row][start] = message.shift();
    }

    return matrix;
};

/**
 * Preprocess a message with padded spacing such that it fits a specific length.
 * Used to pad a message into the squared format needed for ClockWise Cipher.
 */
const padMessage = (message: string[], wantedLength: number) => {
    // Check if message is too short
    if (message.length === wantedLength) return [...message];

    // Find difference
    const diff = wantedLength - message.length;

    // Create padding
    const padding = Array(diff).fill(" ");

    // Pad
    return [...message, ...padding];
};

/**
 * Run the reorder function on the correct number of elements based on the current with of the ring
 * @param message
 * @param width
 * @returns
 */
const reorderSlice = (message: string[], width: number) => {
    if (width == 0 || width === 1 || width === 2) return message;

    // Find cutoff for ringSlice
    const numberToReorder = (width - 1) * 4;

    // Reorder first part
    let slice = message.slice(0, numberToReorder);
    const reorderedSlice = reorder(slice);

    // Return both reordered and remaining parts
    const endOfString = message.slice(numberToReorder);
    return [...reorderedSlice, ...endOfString];
};

/**
 * Reorder a multiple of 4 elements into the right order for the algorithm
 * @param message
 * @returns
 */
const reorder = (message: string[]) => {
    // Generate matrix
    let matrix = [];
    for (let i = 0; i < message.length / 4; i++) {
        matrix.push(new Array(4).fill(-1));
    }

    for (let i = 0; i < message.length; i++) {
        matrix[Math.floor(i / 4)][i % 4] = message[i];
    }

    let reorderedArray = [];
    for (let i = 0; i < 4; i++) {
        for (const row of matrix) {
            reorderedArray.push(row[i]);
        }
    }
    return reorderedArray;
};

/**
 * Extracts an encoded string from a Clockwise Ciphered Matrix by reading row by row
 */
const getEncodedStringFromMatrix = (matrix: Array<Array<string>>) => {
    let encodedString = "";
    for (const row of matrix) {
        for (const char of row) {
            encodedString += char;
        }
    }
    return encodedString;
};

// Reorder function tests
// const test = ["a", "b", "c", "d", "e", "f", "g", "h"];
// const test2 = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
// const test3 = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13","14", "15", "16"];
// const test = ["a", "b", "c", "d", "e", "f", "g", "h"];
// console.log(reorder(test3))

// Reorder slice function tests
// const test = ["a", "b", "c", "d", "e", "f", "g", "h", "i"];
// const width = 3;
// reorderSlice(test, width)

// const test2 = "abcd";
// const test3 = "abcde";
// const test4 = "abcdefgh"
// const test5 = "abcdefghijklmnopqrstu"
// console.log(clockwiseCipher(test5));
