import { countBoomerangs } from "./index"
import theoretically from 'jest-theories';

describe('countBoomerangs', () => {
    const theories = [
        {
            input: [9, 5, 9, 5, 1, 1, 1],
            expected: 2
        },
        {
            input: [5, 6, 6, 7, 6, 3, 9],
            expected: 1
        },
        {
            input: [4, 4, 4, 9, 9, 9, 9],
            expected: 0
        },
        {
            input: [1, 7, 1, 7, 1, 7, 1],
            expected: 5
        }
    ]
    theoretically("should convert the input: '{input}' into the the following: '{expected}'", theories, theory => {
        // Act
        const output = countBoomerangs(theory.input);
        // Assert
        expect(output).toEqual(theory.expected);
    })
})
