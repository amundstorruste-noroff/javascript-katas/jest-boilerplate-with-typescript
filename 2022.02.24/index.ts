/**
 * Checks how many subarrays of length three that have are a Boomerang-sequence.
 * A Boomerang sequence is a sub-array of length 3, with the first and last digits being the same, and the middle being different.
 * @param input 
 * @returns 
 */
export const countBoomerangs = (input: number[]) => {
    let count = 0;
    for (let i=0; i < input.length-2; i++) {
        const slice = input.slice(i,i+3)
        if (isBoomerang(slice)) count++
    }
    return count
}

const isBoomerang = (listOfThree: number[]) => {
    const first = listOfThree[0];
    const second = listOfThree[1];
    const third = listOfThree[2]
    if (first !== third) return false
    if (second === first) return false
    return true;
}

// const testList: number[] = [1, 2, 3]
// const temp = isBoomerang(testList)
// console.log(temp);
