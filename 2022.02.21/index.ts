export const rearrange = (input) => {
    // Stop condition
    if (input === " ") return "" 
    
    let wordList = input.split(" ");
    var reg = new RegExp("[0-9]");

    // Find integers in each word and add to order
    let order = [];
    for (let i = 0; i < wordList.length; i++){
        let integer = wordList[i].match(reg)[0]; 
        order.push(integer-1)
    }
    // Remove integers from words
    let fixedWords = []
    for (const idx of order) {
        let fixedWord = wordList[idx].replace(reg,'');
        fixedWords[idx] = fixedWord;
    }

    // Sort fixed words in right order
    let final = []
    for (let i = 0; i < wordList.length; i++){
        let idx = order[i];
        final[idx] = fixedWords[i]
    }
    return final.join(" ")
}
