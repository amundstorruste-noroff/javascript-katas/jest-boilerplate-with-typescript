import { rearrange } from "./index"
import theoretically from 'jest-theories';

describe('Rearrange', () => {
    const theories = [
        {input: "is2 Thi1s T4est 3a", expected: "This is a Test"},
        {input: "4of Fo1r pe6ople g3ood th5e the2", expected: "For the good of the people"},
        {input: "5weird i2s JavaScri1pt dam4n so3", expected: "JavaScript is so damn weird"},
        {input: " ", expected: ""},
    ]
    theoretically("the input '{input}' is rearranged to '{expected}'", theories, theory => {
        // Act
        const output = rearrange(theory.input);
        // Assert
        expect(output).toBe(theory.expected);
    })
})
