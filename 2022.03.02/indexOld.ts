export const shuffleCount = (cardsInDeck: number) => {
    // Generate deck
    const firstDeck = Array(cardsInDeck).fill(1).map( (_,idx) => idx+1);
    let currentDeck = [...firstDeck];
    let counter = 0;
    do {
        counter++;
        currentDeck = shuffleDeck(currentDeck);
    } while (arrayEquals(currentDeck, firstDeck) == false);
    return counter;
};

/** 
 * Copied this equality thing from StackOverflow
 */
function arrayEquals(ref1: number[], ref2: number[]) {
    let a = [...ref1]
    let b = [...ref2]
    return Array.isArray(a) &&
      Array.isArray(b) &&
      a.length === b.length &&
      a.every((val, index) => val === b[index]);
  }

const shuffleDeck = (deck: number[]) => {
    // Split deck in two halfs
    const half = Math.ceil(deck.length / 2);
    const firstHalf = deck.slice(0, half);
    const secondHalf = deck.slice(-half);

    // Merge together
    deck = mergeDecks(firstHalf, secondHalf)
    return deck;
};

const mergeDecks = (deck1: number[], deck2: number[]) => {
    const mergedDeck = []
    for (var i = 0; i < deck1.length; i++) {
        mergedDeck.push(deck1[i]);
        mergedDeck.push(deck2[i]);
    }
    return mergedDeck;
};

// console.log("hellrup");
// console.log(shuffleCount(8));