import { shuffleCount } from "./index";
import theoretically from "jest-theories";

describe("shuffleCount", () => {
    const theories = [
        {
            input: 8,
            expected: 3
        },
        {
            input: 14,
            expected: 12
        },
        {
            input: 52,
            expected: 8
        },
        {
            input: 444,
            expected: 442
        },
    ];
    theoretically(
        "should convert the input: '{input.string}' into the the following: '{expected}'",
        theories,
        (theory) => {
            // Act
            const output = shuffleCount(theory.input);
            // Assert
            expect(output).toEqual(theory.expected);
        }
    );
});
