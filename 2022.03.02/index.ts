export const shuffleCount = (cardsInDeck: number) => {
    const trackedIndex = 1;
    let currentIndex = 1;
    let counter = 0;
    // Move the index at position 1 around until it's back where we started
    do {
        counter++
        // In first half of deck all indexes are moved their own index to the right
        if( currentIndex < cardsInDeck/2){
            currentIndex += currentIndex;
        }
        // In the second half elements are placed at index (2*n+1) 
        // where n = index - length/2
        else {
            const n = currentIndex - cardsInDeck/2;
            currentIndex = 2*n+1;        
        }
    } while (currentIndex !== trackedIndex)
    // Return counter
    return counter
};
