import { elasticize } from "./index";

test('anna to be annnna', () => {
    let newWord = elasticize("anna")
	expect(newWord).toBe("annnna");
});

// const testWord = "ab" 
// let temp1 = "abc"
// let temp2 = "abcd"
// let temp3 = "abcde"
// let temp4 = "abcdef"
// let temp5 = "anna"
// let temp6 = "kayak"
// let temp7 = "x"


