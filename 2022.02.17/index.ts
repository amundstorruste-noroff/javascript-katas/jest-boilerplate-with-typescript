
export const elasticize = (word) => {
    let wordArray = word.split("");
    let newWord = ""

    for (const [index, value] of wordArray.entries()) {
        let charMultiplier = 1;

        // Check if halfway
        let middleIndex = Math.floor(wordArray.length/2);
        if (index >= middleIndex) charMultiplier = wordArray.length - index;
        else charMultiplier = index + 1;

        newWord += value.repeat(charMultiplier);
    }
    
    return newWord;
}

// const testWord = "ab" 
// let temp1 = "abc"
// let temp2 = "abcd"
// let temp3 = "abcde"
// let temp4 = "abcdef"
// let temp5 = "anna"
// let temp6 = "kayak"
// let temp7 = "x"

// console.log(elasticize(testWord));
// console.log(elasticize(temp1));
// console.log(elasticize(temp2));
// console.log(elasticize(temp3));
// console.log(elasticize(temp4));
// console.log(elasticize(temp5));
// console.log(elasticize(temp6));
// console.log(elasticize(temp7));
