import { maximumProfit } from "./index"

test('profit to be 14', () => {
    let profit = maximumProfit([8, 5, 12, 9, 19, 1])
	expect(profit).toBe(14);
});

test('profit to be 14', () => {
    let profit = maximumProfit([2, 4, 9, 3, 8])
	expect(profit).toBe(7);
});

test('profit to be 14', () => {
    let profit = maximumProfit([21, 12, 11, 9, 6, 3])
	expect(profit).toBe(0);
});