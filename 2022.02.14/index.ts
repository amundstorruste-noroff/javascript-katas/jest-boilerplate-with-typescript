export const maximumProfit = (array) => {
    let smallest = array[0];
    let largest = array[0];
    let profit = 0;
    for (let idx = 0; idx < array.length; idx++) {
        smallest = array[idx];
        largest = Math.max(...array.slice(idx))
        const newProfit = largest - smallest;
        if ( newProfit > profit ) profit = newProfit;
    }

    return profit;
}