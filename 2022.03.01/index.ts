export const caesarCipher = (input: string, shuffles: number) => {
    const charArray = [...input]
    
    const encodedArray = charArray.map( value => {
        let ASCII = value.charCodeAt(0)
        let newASCII = ASCII + shuffles % 26
        
        // A-Z 65-90
        if (65 <= ASCII && ASCII <= 90) {
            if(newASCII > 90) newASCII = 65 + (newASCII - 90) - 1
        }
        // a-z 97-122
        else if (97 <= ASCII && ASCII <= 122) {
            if(newASCII > 122) newASCII = 97 + (newASCII - 122) - 1
        }
        // return anything else as is
        else return value  

        let newVal = String.fromCharCode(newASCII)
        return newVal
    })

    return encodedArray.join("")
}

// const test = "a b c";
// console.log(caesarCipher(test,3))
