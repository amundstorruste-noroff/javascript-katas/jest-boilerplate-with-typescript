import { caesarCipher } from "./index";
import theoretically from "jest-theories";

describe("caesarCipher", () => {
    const theories = [
        {
            input: {
                string: "Always-Look-on-the-Bright-Side-of-Life",
                shuffles: 5
            },
            expected: "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"
        },
        {
            input:{
                string: "A friend in need is a friend indeed",
                shuffles: 20
            },
            expected: "U zlcyhx ch hyyx cm u zlcyhx chxyyx"
        }
    ];
    theoretically(
        "should convert the input: '{input.string}' into the the following: '{expected}'",
        theories,
        (theory) => {
            // Act
            const output = caesarCipher(theory.input.string, theory.input.shuffles);
            // Assert
            expect(output).toEqual(theory.expected);
        }
    );
});
