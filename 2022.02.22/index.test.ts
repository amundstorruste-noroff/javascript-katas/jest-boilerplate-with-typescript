import { noStrangers } from "./index"
import theoretically from 'jest-theories';

describe('noStrangers', () => {
    const theories = [
        {
            input: "See Spot run. See Spot jump. Spot likes jumping. See Spot fly.", 
            expected: [["see", "spot"], []]},
        { 
            input: "One two two three three three", 
            expected: [["three"],[]]
        },
        { 
            input: "acc friend friend friend friend acc acc friend", 
            expected: [["acc"],["friend"]]
        },
        { 
            input: "acc acc2 friend friend friend friend acc acc friend acc2 acc2", 
            expected: [["acc", "acc2"],["friend"]]
        },
    ]
    theoretically("should convert the input: '{input}' into the the following: '{expected}'", theories, theory => {
        // Act
        const output = noStrangers(theory.input);
        // Assert
        expect(output).toEqual(theory.expected);
    })
})
