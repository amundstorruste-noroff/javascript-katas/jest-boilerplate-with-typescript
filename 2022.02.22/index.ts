export const noStrangers = (input) => {
    // Dewalds replace. someString.replace(/[^a-zA-Z' ]/g,'')

    // Trim periods
    const trimmed = input.replaceAll(".", "")
    const array = trimmed.toLowerCase().split(" ")
    
    // Count occurences
    const words = []
    let occurences = []
    for (const word of array) {
        // Track new words
        if (words.includes(word)){
            // Counter
            const idx = words.indexOf(word)
            occurences[idx] += 1
            continue
        } 
        words.push(word);
        occurences.push(1);
    }
    
    // Count friends (5+) and aquantances (3+)
    const aquaintances = []
    const friends = []

    // Zip lists for easier traversal
    const zipped = words.map((word, idx) => [word, occurences[idx]]);
    for (const [word, count] of zipped) {
        if (count >= 5) friends.push(word)
        else if (count >= 3) aquaintances.push(word)
    }

    return [aquaintances, friends]
}